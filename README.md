This is a repository for codes to control chillers

* read.py: To read values from a SMART H150-1000
* write.py: To set the target temperature for a SMART H150-1000
* socket: Codes for socket communication

The communication is via RS485; hence, it is supposed for
a computer to send the serial signal in RS485. Usually,
it can be done using a USB-to-RS485 convertor.

The codes are written in Python3.
The codes have been tested with Linux (Scientific Linux6 and Rapsbian).
In the MEG II experiment, it is supposed that a Raspberry Pi (megpi5.psi.ch)
controls two SMART H150-1000 chillers for pTC.


## Usage ##
To read from a chiller
```
$ ./read.py [address]
```
To set a target temperature
```
$ ./write.py [address] [temp in degC]
then type 'y'
```
[address] is the address set for each device.
(You can set an address between 1 and 8 from the touch panel of the device.)
To control multiple devices, different address must be set for each device.


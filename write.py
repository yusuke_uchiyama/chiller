#!/usr/bin/env python
import serial
import struct
import array
import sys
import read

#pyenv
# $ pyenv local 3.6.1

# usb port is /dev/ttyUSB0, but user has no permission
# lsudb // check the device
# echo 'SUBSYSTEM=="tty", ATTRS{idVendor}=="067b", ATTRS{idProduct}=="04bb", MODE="0666"' | sudo tee /etc/udev/rules.d/50-usb-serial.rules
# sudo udevadm control --reload-rules

# Usage:
# $ ./wirte.py [address] [temperature]

if len(sys.argv) != 3:
    print("Set address as the first argument and target temperature (in degC) as second.")
    print("$ ./write.py [address] [temp]")
    exit()

print("===================================")


use_port = '/dev/ttyUSB0'

_serial = serial.Serial(use_port)
_serial.baudrate = 9600
_serial.parity = serial.PARITY_EVEN
_serial.bytesize = serial.EIGHTBITS
_serial.stopbits = serial.STOPBITS_ONE
_serial.timeout = 5 #sec

address = int(sys.argv[1]) #0x03
debug = False

# First read the current setting
reply = read.read_setting(_serial, address, debug)
print('')

# Writing the target temperature

target = float(sys.argv[2])

print(f'You are going to set the temperature to {target} degC for chiller address {address}.')
if target < 8 or target > 25:
    print(f"Are you really sure? The target value looks strange ({target})")
    exit()
print("Are you sure? (y or n)")
str = input()
if str != 'y':
    print("cancel")
    exit()

print(f"Writing the target temperature {target}degC")

target *= 10
targetInt = int(target)

commands = [ 0xFC, 0xFC, # Start character
             address,       # Address
             192,        # Function code for writing
             0, 10, 0, 0, 69, 64, 0,  # Data for 32 bytes
             150,        # Temperature setting (1 decimal floating point)
             0, 44, 0, 5, 1, 94, 0, 80, # other settings, don't edit!
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0x00,       # Check byte to be calcuated later
             0xFA ]      # End byte
# Copy the current setting
for i in range(4, 30):
    commands[i] = reply[i]

# Set the target temp.
commands[10] = targetInt>>8;
commands[11] = targetInt & 0b0000000011111111

# check sum
check = 0
for n in range(2,30):
    check += commands[n]
check &= 0b11111111
commands[30] = check

# use byte() or bytearray() to convert into binary
b_data = bytearray(commands)

if debug:
    print(b_data)
_serial.write(b_data)
_serial.flush()


#rx = _serial.readline()
rx = _serial.read(8) # read 8 bytes
if debug:
    print("rx: ", rx)
try:
    reply = struct.unpack('8B', rx)
except:
    print('Failed to write')
    exit()
if debug:
    print(reply)

print('')

# Read the new setting
read.read_setting(_serial, address, debug)
print("===================================")

_serial.close()

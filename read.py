#!/usr/bin/env python
import serial
import struct
import array
import sys

#pyenv
# $ pyenv local 3.6.1

# usb port is /dev/ttyUSB0, but user has no permission
# lsudb // check the device
# echo 'SUBSYSTEM=="tty", ATTRS{idVendor}=="067b", ATTRS{idProduct}=="04bb", MODE="0666"' | sudo tee /etc/udev/rules.d/50-usb-serial.rules
# sudo udevadm control --reload-rules

# Usage:
# $ ./read.py [address]

def read_setting(_serial, address, debug):
    # Read setting
    print("Reading setting values...")
    commands = [ 0xFC, 0xFC, # Start character
                 address,       # Address
                 161,       # Function code
                 0x00, 0x00, # Data
                 0x00,       # Check byte to be calculated later
                 0xFA ]      # End byte
    # check sum
    check = 0
    for n in range(2,6):
        check += commands[n]
    check &= 0b11111111
    commands[6] = check

    # use byte() or bytearray() to convert into binary
    b_data = bytearray(commands)
    if debug:
        print(b_data)
    _serial.write(b_data)
    _serial.flush()


    #rx = _serial.readline()
    rx = _serial.read(32) # read 32 bytes
    #rx = _serial.read_all()
    reply = struct.unpack('32B', rx)
    if debug:
        print("rx: ", rx)
        print(reply)
        print(bin((reply[6]<<8) + reply[7]))
        print(bin((reply[8]<<8) + reply[9]))
        print((reply[10]<<8) + reply[11])
        print((reply[12]<<8) + reply[13])
        print((reply[14]<<8) + reply[15])
        print((reply[16]<<8) + reply[17])
        print((reply[18]<<8) + reply[19])
        print((reply[20]<<8) + reply[21])
        print((reply[22]<<8) + reply[23])

    # check check-sum
    check = 0
    for n in range(2,30):
        check += reply[n]
    check &= 0b11111111
    if reply[30] != check:
        print("Check-sum failed.")
        exit()

    # if reply[6] | reply[7]:
    #     print("ERROR STATUS!!!!!")
    #     print(bin((reply[6]<<8) + reply[7]))

    # decode "Dot data"
    decimals = [0] * 8

    for i in range(0,8):
        decimals[i] = ((reply[8]<<8) + reply[9]) >> (14 - i*2) & 0b11
        if debug:
            print(f'decimal {i}: {decimals[i]}')

    # decode data
    values = [0] * 8
    for i in range(0, 8):
        values[i] = ((reply[10 + 2*i]<<8) + reply[11 + 2*i])
        for j in range(0, decimals[i]):
            values[i] /= 10

    # Print results
    print("-----------------------------------")
    print(f'Target Temperature:\t {values[0]} degC')
    print(f'Pressure ALM:\t\t {values[1] * 0.0689476:.3g} bar') # PSI to bar
    print(f'Temperature high limit:\t {values[3]} degC')
    print(f'Temperature low limit:\t {values[4]} degC')
    print(f'Flow ALM:\t\t {values[2]}')

    return reply

def read_values(_serial, address, debug):

    # Read the current values
    print("Reading current values...")
    commands = [ 0xFC, 0xFC, # Start character
                 address,       # Address
                 0xA0,       # Function code
                 0x00, 0x00, # Data
                 0x00,       # Check byte to be calculated later
                 0xFA ]      # End byte
    # check sum
    check = 0
    for n in range(2,6):
        check += commands[n]
    check &= 0b11111111
    commands[6] = check

    # use byte() or bytearray() to convert into binary
    b_data = bytearray(commands)
    if debug:
        print(b_data)
    _serial.write(b_data)
    _serial.flush()

    rx = _serial.read(32) # read 32 bytes
    reply = struct.unpack('32B', rx)
    if debug:
        print("rx: ", rx)
        print(reply)
        print(bin((reply[8]<<8) + reply[9]))
        print((reply[10]<<8) + reply[11])
        print((reply[12]<<8) + reply[13])
        print((reply[14]<<8) + reply[15])
        print((reply[16]<<8) + reply[17])
        print((reply[18]<<8) + reply[19])
        print((reply[20]<<8) + reply[21])
        print((reply[22]<<8) + reply[23])


    # check check-sum
    check = 0
    for n in range(2, 30):
        check += reply[n]
    check &= 0b11111111
    if reply[30] != check:
        print("Check-sum failed.")
        exit()

    if reply[6] | reply[7]:
        print("ERROR STATUS!!!!!")
        print(bin((reply[6]<<8) + reply[7]))

    # decode "Dot data"
    decimals = [0] * 8

    for i in range(0,8):
        decimals[i] = ((reply[8]<<8) + reply[9]) >> (14 - i*2) & 0b11
        if debug:
            print(f'decimal {i}: {decimals[i]}')

    # decode data
    values = [0] * 8
    for i in range(0, 8):
        values[i] = ((reply[10 + 2*i]<<8) + reply[11 + 2*i])
        for j in range(0, decimals[i]):
            values[i] /= 10

    # Print results
    print("-----------------------------------")
    print(f'Temperature:\t\t {values[0]} degC')
    print(f'Pressure:\t\t {values[1] * 0.0689476:.3g} bar') # PSI to bar
    print(f'Running time:\t\t {values[4]} hours')


def main():

    if len(sys.argv) != 2:
        print("Set address as the first argument.")
        print("$ ./read.py [address]")
        exit()

    print("===================================")


    use_port = '/dev/ttyUSB0'
    
    _serial = serial.Serial(use_port)
    _serial.baudrate = 9600
    _serial.parity = serial.PARITY_EVEN
    _serial.bytesize = serial.EIGHTBITS
    _serial.stopbits = serial.STOPBITS_ONE
    _serial.timeout = 5 #sec

    address = int(sys.argv[1]) #0x03
    debug = False

    read_setting(_serial, address, debug)
    print('')
    read_values(_serial, address, debug)


    print("===================================")

    _serial.close()


if __name__ == "__main__":
    main()

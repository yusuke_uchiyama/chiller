#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# purpose: simple sample of server program using socket via UDP
# useage: $ python3 server_udp.py

import socket
import time
import datetime
from SMART_H150 import SMART_H150

def main(host, port, buffer_size):
    print('---------------- server_udp.main() start ----------------')
    print('host:', host)
    print('port:', port)
    print('buffer_size:', buffer_size)

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # create datagram socket, i.e., without requiring connection establishment
    print('SOCKET CREATED')
    print('s:', s)

    s.bind((host, port))  # bind socket, only needed in server, not in client

    chiller_controller = SMART_H150()
    
    print('Ctrl+C to quit')
    while True:
        try:
            command, client_address = s.recvfrom(buffer_size)  # receive command
            time_stamp = datetime.datetime.now()  # get timestamp
            command = command.decode(encoding='utf-8')  # decodde command
            cmd = command.strip('\r')

            values = []
            reply = ''
            if 'READ' in command:
                try:
                    address = int(command.split()[1])
                    values = chiller_controller.read(address)
                    reply = '\n'.join(map(str, values))
                    if int(values[15]):
                        print(f'{time_stamp} COMMAND {cmd}: Error status {values[15]}')
                except Exception as e:
                    print(f'{time_stamp} COMMAND {cmd}: {e}')
                    reply = 'failed'

            elif 'SET' in command:
                try:
                    address = int(command.split()[1])
                    value = float(command.split()[2])
                    chiller_controller.write(value, address, False)
                    reply = 'success'
                except Exception as e:
                    print(f'{time_stamp} COMMAND {cmd}: {e}')                    
                    reply = 'failed'

            time.sleep(0.5)  # sec, wait until client is ready to receive, to computer, 0.5 sec equals an eternity
            s.sendto((reply).encode(encoding='utf-8'), client_address)  # notify client that command has been received

        except KeyboardInterrupt:  # i.e., Ctrl+C
            s.close()
            print('SOCKET CLOSED')
            break

    print('---------------- server_udp.main() end ----------------')


if __name__ == '__main__':

    host = 'megpi5.psi.ch' # '127.0.0.1'  # local host
    port = 51556  # use 49152 or higher
    buffer_size = 1024  # bytes, this must be larger enough than message size
    main(host, port, buffer_size)

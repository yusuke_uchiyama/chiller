#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# purpose: simple sample of client program using socket via UDP
# useage: $ python3 client_udp.py

import socket
from command_parser import CommandParser


def main(host, port, buffer_size):
    print('---------------- client_udp.main() start ----------------')
    print('host:', host)
    print('port:', port)
    print('buffer_size:', buffer_size)

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # create datagram socket, i.e., without requiring connection establishment
    print('SOCKET CREATED')
    print('s:', s)

    command_parser = CommandParser()
    command_parser.help()

    while True:
        try:
            command = input()

            if command.isdecimal() and int(command) in command_parser.command_send:  # check if command is valid or invalid

                if int(command) != 0:  # 0 means quit client
                    command = command_parser.command_send[int(command)]
                    send_length = s.sendto(command.encode('utf-8'), (host, port))  # send command to server, send_length is in unit of byte
                    response, server_address = s.recvfrom(buffer_size)  # receive response from server
                    print('RETURN:', response.decode(encoding='utf-8'))

                else:  # i.e., finish client
                    s.close()
                    print('SOCKET CLOSED')
                    break

            else:
                print('INVALID COMMNAD')

        except KeyboardInterrupt:  # i.e., Ctrl+C
            s.close()
            print('SOCKET CLOSED')
            break

    print('---------------- client_udp.main() end ----------------')


if __name__ == '__main__':

    host = 'megpi5.psi.ch' # '127.0.0.1'  # local host
    port = 51556  # use 49152 or higher
    buffer_size = 1024  # bytes, this must be larger enough than message size
    main(host, port, buffer_size)

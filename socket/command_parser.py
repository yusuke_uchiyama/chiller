#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# purpose: command table, modify this code and list practical functions, e.g., control chiller or adc or ...
# useage: $ python3 command_parser.py

class CommandParser(object):

    def __init__(self):
        print('---------------- CommandParser.__init__() start ----------------')

        # use from client
        self.command_send = {0: 'quit',
                             1: 'READ 2',
                             2: 'READ 3',
                             3: 'SET'}  # in practical, here, write easy-to-press options and functions name or something

        # use from server
        self.command_receive = {'end': 'end',
                                'READ 2': '紫野行き紫野行き野守は見ずや君が袖振る',
                                'READ 3': '照れる春日にひばり上がり心悲しも独し思へば',
                                'SET': '象山の際の木末にはここだも騒ぐ鳥の声かも'}  # in practical, here, write functions name and actual functions
        print('---------------- CommandParser.__init__() end ----------------')

    def help(self):
        print('---------------- CommandParser.help() start ----------------')
        for i in self.command_send:
            print(str(i) + ':', self.command_send[i])
        print('---------------- CommandParser.help() end ----------------')


if __name__ == '__main__':

    CommandParser = CommandParser()
    CommandParser.help()

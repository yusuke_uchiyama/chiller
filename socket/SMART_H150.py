#!/usr/bin/env python
import serial
import struct
import array
import sys

class SMART_H150(object):
    def __init__(self):
        self.use_port = '/dev/ttyUSB0'
        
        self.serial = serial.Serial(self.use_port)
        self.serial.baudrate = 9600
        self.serial.parity = serial.PARITY_EVEN
        self.serial.bytesize = serial.EIGHTBITS
        self.serial.stopbits = serial.STOPBITS_ONE
        self.serial.timeout = 1 #sec
        

    def read_setting(self, address, debug):
        if self.serial.isOpen() is False:
            self.serial.open()

        # Read setting
        if debug:
            print("Reading setting values...")
        commands = [ 0xFC, 0xFC, # Start character
                     address,       # Address
                     161,       # Function code
                     0x00, 0x00, # Data
                     0x00,       # Check byte to be calculated later
                     0xFA ]      # End byte
        # check sum
        check = 0
        for n in range(2,6):
            check += commands[n]
        check &= 0b11111111
        commands[6] = check

        # use byte() or bytearray() to convert into binary
        b_data = bytearray(commands)
        if debug:
            print(b_data)
        self.serial.write(b_data)
        self.serial.flush()

        rx = self.serial.read(32) # read 32 bytes
        try:
            reply = struct.unpack('32B', rx)
        except Exception as e:
            raise Exception(f'Failed in reading setting from address {address}')

        if debug:
            print("rx: ", rx)
            print(reply)
            print(bin((reply[6]<<8) + reply[7]))
            print(bin((reply[8]<<8) + reply[9]))
            print((reply[10]<<8) + reply[11])
            print((reply[12]<<8) + reply[13])
            print((reply[14]<<8) + reply[15])
            print((reply[16]<<8) + reply[17])
            print((reply[18]<<8) + reply[19])
            print((reply[20]<<8) + reply[21])
            print((reply[22]<<8) + reply[23])

        # check check-sum
        check = 0
        for n in range(2,30):
            check += reply[n]
        check &= 0b11111111
        if reply[30] != check:
            print("Check-sum failed.")

        # decode "Dot data"
        decimals = [0] * 8

        for i in range(0,8):
            decimals[i] = ((reply[8]<<8) + reply[9]) >> (14 - i*2) & 0b11
            if debug:
                print(f'decimal {i}: {decimals[i]}')

        # decode data
        values = [0] * 8
        for i in range(0, 8):
            values[i] = ((reply[10 + 2*i]<<8) + reply[11 + 2*i])
            for j in range(0, decimals[i]):
                values[i] /= 10
        values[1] *= 0.0689476 #pressure from PSI to bar

        if debug:
            # Print results
            print("-----------------------------------")
            print(f'Target Temperature:\t {values[0]} degC')
            print(f'Pressure ALM:\t\t {values[1]:.3g} bar')
            print(f'Temperature high limit:\t {values[3]} degC')
            print(f'Temperature low limit:\t {values[4]} degC')
            print(f'Flow ALM:\t\t {values[2]}')

        return reply, values



    def read_values(self, address, debug):
        if self.serial.isOpen() is False:
            self.serial.open()

        # Read the current values
        if debug:
            print("Reading current values...")
        commands = [ 0xFC, 0xFC, # Start character
                     address,       # Address
                     0xA0,       # Function code
                     0x00, 0x00, # Data
                     0x00,       # Check byte to be calculated later
                     0xFA ]      # End byte
        # check sum
        check = 0
        for n in range(2,6):
            check += commands[n]
        check &= 0b11111111
        commands[6] = check

        # use byte() or bytearray() to convert into binary
        b_data = bytearray(commands)
        if debug:
            print(b_data)
        self.serial.write(b_data)
        self.serial.flush()

        rx = self.serial.read(32) # read 32 bytes
        try:
            reply = struct.unpack('32B', rx)
        except Exception as e:
            raise Exception(f'failed in reading values from addres {address}')

        if debug:
            print("rx: ", rx)
            print(reply)
            print(bin((reply[8]<<8) + reply[9]))
            print((reply[10]<<8) + reply[11])
            print((reply[12]<<8) + reply[13])
            print((reply[14]<<8) + reply[15])
            print((reply[16]<<8) + reply[17])
            print((reply[18]<<8) + reply[19])
            print((reply[20]<<8) + reply[21])
            print((reply[22]<<8) + reply[23])


        # check check-sum
        check = 0
        for n in range(2, 30):
            check += reply[n]
        check &= 0b11111111
        if reply[30] != check:
            raise Exception("Check-sum failed.")

        status = (reply[6]<<8) + reply[7]
        if debug and status:
            print("ERROR STATUS!!!!!")
            print(bin(status))

        # decode "Dot data"
        decimals = [0] * 8

        for i in range(0,8):
            decimals[i] = ((reply[8]<<8) + reply[9]) >> (14 - i*2) & 0b11
            if debug:
                print(f'decimal {i}: {decimals[i]}')

        # decode data
        values = [0] * 8
        for i in range(0, 8):
            values[i] = ((reply[10 + 2*i]<<8) + reply[11 + 2*i])
            for j in range(0, decimals[i]):
                values[i] /= 10
        values[1] *= 0.0689476 # pressure in bar from PSI
        values[7] = status

        if debug:
            # Print results
            print("-----------------------------------")
            print(f'Temperature:\t\t {values[0]} degC')
            print(f'Pressure:\t\t {values[1]:.3g} bar')
            print(f'Running time:\t\t {values[4]} hours')

        return values


    def read(self, address, debug=False):
        if self.serial.isOpen() is False:
            self.serial.open()
            
        reply = []
        values = []
        try:
            reply, values = self.read_setting(address, debug)
        except Exception as e:
            raise e
        try:
            values += self.read_values(address, debug)
        except Exception as e:
            raise e

        self.serial.close()
        
        return values

    def write(self, target, address, debug=False):
        if self.serial.isOpen() is False:
            self.serial.open()

        try:
            target = float(target)
        except ValueError:
            raise Exception(f'Invalid input {target}')

        # First read the current setting
        reply = []
        setting = []
        try:
            reply, setting = self.read_setting(address, debug)
        except Exception as e:
            raise e

        # Writing the target temperature
        if target < 8 or target > 25:
            raise Exception(f'Invalid input {target}. It should be in [8, 25].')

        print(f"Writing the target temperature {target}degC to address {address}")

        target *= 10
        targetInt = int(target)

        commands = [ 0xFC, 0xFC, # Start character
                     address,       # Address
                     192,        # Function code for writing
                     0, 10, 0, 0, 69, 64, 0,  # Data for 32 bytes
                     150,        # Temperature setting (1 decimal floating point)
                     0, 44, 0, 5, 1, 94, 0, 80, # other settings, don't edit!
                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                     0x00,       # Check byte to be calcuated later
                     0xFA ]      # End byte
        # Copy the current setting
        for i in range(4, 30):
            commands[i] = reply[i]

        # Set the target temp.
        commands[10] = targetInt>>8;
        commands[11] = targetInt & 0b0000000011111111

        # check sum
        check = 0
        for n in range(2,30):
            check += commands[n]
        check &= 0b11111111
        commands[30] = check

        # use byte() or bytearray() to convert into binary
        b_data = bytearray(commands)

        if debug:
            print(b_data)
        self.serial.write(b_data)
        self.serial.flush()


        rx = self.serial.read(8) # read 8 bytes
        if debug:
            print("rx: ", rx)
        try:
            reply = struct.unpack('8B', rx)
        except:
            raise Exception('Failed to write')

        if debug:
            print(reply)


        # Read the new setting
        reply, setting = self.read_setting(address, debug)
        if setting[0] is not target:
            raise Exception('Falied to write')
        

        self.serial.close()

    
def main():

    if len(sys.argv) != 2:
        print("Set address as the first argument.")
        print("$ ./read.py [address]")
        exit()

    print("===================================")

    chiller = SMART_H150()

    address = int(sys.argv[1])
    debug = False
    values =  []
    try:
        values = chiller.read(address, debug)        
    except Exception as e:
        print(e)
        exit()
    
    print("===================================")
    print(values)
    
    chiller.serial.close()


if __name__ == "__main__":
    main()
